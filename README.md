# MedicineKick SourceMod Plugin

If you need this plugin, you need SourceMod. Help them meet their monthly goal [here](http://sourcemod.net/donate.php). Seriously, if you need any of this just consider, the SourceMod community need some love too.

Thanks :)

## License
MedicineKick a SourceMod L4D2 Plugin
Copyright (C) 2017  Victor "NgBUCKWANGS" Gonzalez

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

## About
MedicineKick will reverse the vote to kick if the kicker can't target the kickee.

### Installing MedicineKick
1. Drop medicinekick.smx into its proper place
	- .../left4dead2/addons/sourcemod/plugins/medicinekick.smx
2. Load up MedicineKick
  - ```sm_rcon sm plugins load medicinekick```
  - OR restart the server
3. Customize MedicineKick (Generated on first load)
	- .../left4dead2/cfg/sourcemod/medicinekick.cfg

### Uninstalling MedicineKick
1. Remove .../left4dead2/addons/sourcemod/plugins/medicinekick.smx
2. Remove .../left4dead2/cfg/sourcemod/medicinekick.cfg

### Disabling MedicineKick
1. Move medicinekick.smx into plugins/disabled
2. ```sm_rcon sm plugins unload medicinekick```

## Thanks
Big thanks to [Vote Blocker](http://psbj.github.io/sourcemod/voteblocker) by [Phil Bradley](http://steamcommunity.com/groups/voteblocker) for insight on how to approach the problem.

## Reaching Me
I love L4D2, developing, testing and running servers more than I like playing the game. Although I do enjoy the game and it is undoubtedly my favorite game, it is the community I think I love the most. It's always good to meet new people with the same interest :)

- [My Steam Profile](http://steamcommunity.com/id/buckwangs/)
- [My MedicineKick GitLab Page](https://gitlab.com/vbgunz/MedicineKick)
